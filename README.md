# CRUD metadaloģijas demonstrācija WEB vidē

Programmatūra paredzēta CRUD metodoloģijas demonstrācijai WEB vidē. Tā tika izveidota mācību ietvaros lai demonstrētu prasmes, veidojot servisus, kuri izpilda CRUD galvenos principus:
- ievietot datus
- atjaunot eksistējošus datus
- dzēst eksistējošus datus
- iegūt eksistējošu datu unikālos identifikātorus
- iegūt eksistējošos datus pēc unikālā identifikatora
- iegūt visus eksistējošos datus
- iegūt eksistējošos datus pēc noteiktiem parametriem kā: nosaukums, datums, vārds, uzvārds, talr. nr., ē-pasts u.c.
Visi dati no datubāzes tiek tabulas formas veidā atrādīti WEB vietnē. Katram datubāzes laukam ir savs attiecīgais page.php skripts, kas nodrošina lietotāja datu apstrādi.

Lai šo programmatūru pielietotu ir nepieciešama SQL datubāze, kuru var izveidot palaižot shop.sql skripta kodu, kā arī WEB serveris. Šī programmatūra tika veidota uz Apache WEB serveera, tāpēc ka tas nodrošina stabilu, dinamisku WEB bāzētas programmatūras uzturēšanu.

Programmas izstrādes ietvaros tika pielietotas šādas tehnoloģijas:
- PHP 7
- MySql
- CSS

PHP tika izvirzīta kā nepieciešamā programmēšanas valoda priekš programmatūras izstrādes, jo priekš WEB izstrādes ir viena no visdokumntātākām programmēšanas valodām, kurai ir priekšrocības tās atkļūdošanas un problēmu risināšanas aspektos.

Datubāzes datu apstrādei tika pilietots MySQL nevis PostgreSQL, jo projekta ietvaros tā lietošana nebija nepieciešama, bet programmatūru būtu labāk izveidot uz PostgreSQL lai ātrāk apstrādāt lielas datu kopas, sarežģītus vaicājumus un lasīšanas-rakstīšanas darbības.

Tā kā tas nebija liels projekts, bet tieši projekts mācību ietvaros, tā vizuālajam noformējumam nebija likt tik liels uzsvars kā pašas programmas funkcionalitātei, bet, ja vizuālais noformējums un adaptivitāte būtu nepieciešama, tad bootstrap 5 frameworks būtu vislabākā iespēja, jo tā spētu nodrošināt viedu adaptāciju starp vairākām platformām.


